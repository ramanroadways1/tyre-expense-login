<?php
require_once("_connect.php");

$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id'])); 

if(empty($id)){
	AlertRightCornerError("Trip Id not found !");
	exit();
}

$qry = Qry($conn,"SELECT tno,branch,from_station,to_station,cash FROM dairy.trip WHERE id='$id'");
	
if(numRows($qry)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}

$_SESSION['exp_trip_id'] = $id;

$row = fetchArray($qry);
?>

		<div class="row">
			
			<div class="form-group col-md-6">
				<label>Branch <font color="red"><sup>*</sup></font></label>
				<input type="text" class="form-control" value="<?php echo $row["branch"]; ?>" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Date <font color="red"><sup>*</sup></font></label>
				<input type="text" class="form-control" value="<?php echo date("d-m-Y"); ?>" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red"><sup>*</sup></font></label>
				<input type="number" class="form-control" name="amount" required="required">
			</div> 
			
			<div class="form-group col-md-6">
				<label>Select Expense <font color="red"><sup>*</sup></font></label>
				<select name="exp_id" class="form-control" required="required">
					<option value="">--select expense--</option>
					<option value="2">TRUCK TYRE EXPS.</option>
				</select>
			</div>
			
			<div class="form-group col-md-6">
				<label>Slip/Parchi Number <font color="green"><sup>(optional)</sup></font></label>
				<input type="text" class="form-control" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" name="slip_no">
			</div>
			
			<div class="form-group col-md-6">
				<label>Invoice <font color="red"><sup>*</sup></font></label>
				<input type="file" class="form-control" name="bill[]" multiple="multiple" required="required">
			</div>
			
			<input type="hidden" name="trip_id" value="<?php echo $id; ?>">
			
			<div class="form-group col-md-12">
				<label>Narration <font color="red"><sup>*</sup></font></label>
				<textarea class="form-control" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" name="narration" required="required"></textarea>
			</div>
		</div>
	

<script>
$('#modal_exp_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 

