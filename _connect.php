<?php
include($_SERVER['DOCUMENT_ROOT']."/_connect.php");

session_start();

if(!isset($_SESSION['d_tyre_exp']))
{
	session_destroy();
	echo "<script>
		window.top.location.href='https://rrpl.online/';
	</script>";
	exit();
}

date_default_timezone_set('Asia/Kolkata');

$page_name=$_SERVER['REQUEST_URI'];
$page_url=$_SERVER['REQUEST_URI'];

$timestamp = date("Y-m-d H:i:s");

function AlertError($msg)
{
	 // warning // error
	echo "<script>
	Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>$msg</font>',});
	$('#loadicon').fadeOut('slow');
	</script>";
}

function AlertErrorTopRight($msg)
{
	 // warning // error
	echo "<script>
	Swal.fire({
			position: 'top-end',
			icon: 'warning',
			html: '<font size=\'2\' color=\'black\'>$msg</font>',
			showConfirmButton: false,
			timer: 3000
		})
	$('#loadicon').fadeOut('slow');
	</script>";
}

function ShowErrorMsg($conn)
{
	echo "<script>
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>System error. Contact system-admin</font>',});
		$('#loadicon').fadeOut('slow');
	</script>";
}

function ErrorAlert($msg)
{
	AlertError("Oops..". $msg);
}


function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function OkAlert($msg)
{
	echo "<script>
	Swal.fire({
		position: 'top-end',
		icon: 'success',
		html: '<font size=\'2\' color=\'black\'>$msg</font>',
		showConfirmButton: false,
		timer: 1500
	})
	$('#loadicon').fadeOut('slow');
	</script>";
}

function AlertRightCornerError($msg)
{
	 // warning // error
	echo "<script>
	$('body').css('cursor', 'not-allowed');
	Swal.fire({
			position: 'top-end',
			icon: 'warning',
			html: '<font size=\'2\' color=\'black\'>$msg</font>',
			showConfirmButton: false,
			timer: 3000
		})
		$('#loadicon').fadeOut('slow');
	</script>";
}

function AlertRightCornerSuccess($msg)
{
	 // warning // error
	echo "<script>
	Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: '<font size=\'2\' color=\'black\'>$msg</font>',
			showConfirmButton: false,
			timer: 3000
		})
	$('#loadicon').fadeOut('slow');
	</script>";
}

function AlertRightCornerSuccessFadeFast($msg)
{
	 // warning // error
	echo "<script>
	Swal.fire({
			position: 'top-end',
			icon: 'success',
			html: '<font size=\'2\' color=\'black\'>$msg</font>',
			showConfirmButton: false,
			timer: 1000
		})
	$('#loadicon').fadeOut('slow');
	</script>";
}
?>