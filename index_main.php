<?php
include("_header.php");

$date = date("Y-m-d");

$days_prev = date('Y-m-d',strtotime("yesterday"));

$get_trips = Qry($conn,"SELECT id FROM dairy.trip WHERE date(date)='$date'");

$total_trips = numRows($get_trips);

$get_tyre_exp = Qry($conn,"SELECT id FROM dairy.trip_exp WHERE date(timestamp)='$date' AND exp_code='TR00655'");

$total_tyre_exp = numRows($get_tyre_exp);

$tyre_exp_amt_today = Qry($conn,"SELECT COALESCE(sum(amount),0) as amount FROM dairy.trip_exp WHERE date(timestamp)='$date' AND exp_code='TR00655'");

$row_tyre_exp_amt_today = fetchArray($tyre_exp_amt_today);

$amt_tyre_exp_today = $row_tyre_exp_amt_today['amount']; 

$tyre_exp_yes = Qry($conn,"SELECT COALESCE(count(id),0) as count,COALESCE(SUM(amount),0) as amount FROM dairy.trip_exp WHERE date(timestamp)='$days_prev' AND exp_code='TR00655'");

$row_tyre_exp_yes = fetchArray($tyre_exp_yes);
?>

<div class="content-wrapper">
       <section class="content-header">
          <h1 style="font-size:16px;">Dashboard</h1>
       </section>

		<section class="content">
          <div class="row">
            <div onclick="GetData('trips_today')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $total_trips; ?></h3>
                  <p>Trips Today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
            
			<div onclick="GetData('market_bilty')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $total_tyre_exp; ?></h3>
                  <p>Tyre Exp. Today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
			
            <div onclick="GetData('hisab_today')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-yellow">
                <div class="inner">
                 <h3><?php echo $amt_tyre_exp_today; ?></h3>
                  <p>Tyre Exp. Amount Today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
			
            <div onclick="GetData('free_vehicle')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-red">
                <div class="inner">
                 <h3><?php echo $row_tyre_exp_yes['count']. "<span style='font-size:15px'>($row_tyre_exp_yes[amount])</span>"; ?></h3>
                   <p>Tyre Exp. Yesterday</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->
     
<script>	 
function GetData(report_type)
{
	// $('#loadicon').show();
		// jQuery.ajax({
			// url: "modal_show_report.php",
			// data: 'report_type=' + report_type,
			// type: "POST",
			// success: function(data) {
			// $("#load_modal_div").html(data);
			// },
			// error: function() {}
		// });
}
</script>	

<div id="load_modal_div"></div> 
	 
<?php include("_footer.php"); ?>
