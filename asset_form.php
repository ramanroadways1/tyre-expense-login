<?php
include("_header.php");
?>
<script>
$(function() {
		$("#hisab_tno").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#hisab_tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#hisab_tno").val('');
			$("#hisab_tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">View Asset Form : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label>Vehicle No.</label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" class="form-control" id="hisab_tno" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px" onclick="SearchHisab()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  

<script>	
function SearchHisab()
{
	var tno = $('#hisab_tno').val();
	
	if(tno=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_asset_form.php",
				data: 'tno=' + tno,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}
</script>

<?php include("_footer.php") ?>
