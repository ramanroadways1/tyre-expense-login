<?php
include("_header.php");
?>
<script>
$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists.</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form2").on('submit',(function(e) {
$("#loadicon").show();
$("#exp_submit").attr("disabled",true);
e.preventDefault();
	$.ajax({
	url: "./save_expense.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_exp_form").html(data);
		// $("#exp_submit").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script> 

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Running Trips : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">		
						<div class="form-group col-md-3">
							<label>Vehicle Number <sup><font color="red">*</font></sup></label>
							<input id="own_tno" name="own_tno" autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');ResetIp('tno')" 
							type="text" class="form-control" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="GetTrips()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn">
							<i id="search_icon" class="fa fa-search" aria-hidden="true"></i> 
							<i id="spinner_icon" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> &nbsp; Get Trips !</button>
						</div>
						
					<div style="overflow:auto" id="result_div" class="table-responsive form-group col-md-12">
					</div>					
				</div> 
				</div> 
                </div><!-- /.box-body --> 
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>

<div id="func_result"></div>  
<div id="modal_result_div"></div>  

<script>
function GetTrips()
{
	var tno = $('#own_tno').val();
	
	if(tno=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter Vehicle number first !</font>',});
	}
	else
	{
		$('#search_icon').hide();
		$('#spinner_icon').show();
		jQuery.ajax({
				url: "./_load_running_trips.php",
				data: 'tno=' + tno,
				type: "POST",
				success: function(data) {
					$("#result_div").html(data);
				},
				error: function() {}
		});
	}
}

function ViewTrip(id)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "./modal_view_trip.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#modal_result_div").html(data);
		},
		error: function() {}
	});
}

function ExpenseEntry(id)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "./modal_trip_expense.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#exp_modal_div").html(data);
		},
		error: function() {}
	});
}
</script>

<div id="result_advance_form"></div>

<button id="modal_exp_btn" style="display:none" data-toggle="modal" data-target="#ModalExp"></button>

<form id="Form2" autocomplete="off">
<div class="modal fade" id="ModalExp" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Expense ENTRY : </span>
			</div>
	<div class="modal-body" id="exp_modal_div">
</div>
	
		<div class="modal-footer">
			<button type="submit" name="submit" id="exp_submit" class="btn btn-sm btn-success">Submit</button>
			<button type="button" id="exp_modal_close_btn" onclick="GetTrips()" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
		</div>
	 
      </div>
    </div>
</div>
</form>

<div id="result_exp_form"></div>
 
