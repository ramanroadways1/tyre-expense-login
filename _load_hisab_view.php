<?php
require_once '_connect.php'; 

$trip_no = escapeString($conn,($_POST['trip_no']));
$tno = escapeString($conn,($_POST['tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($trip_no!='')
{
	$sql = Qry($conn,"SELECT 
	(SELECT date(date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
	(SELECT date(end_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
	(SELECT from_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
	(SELECT to_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station',
	(SELECT date(hisab_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_date',
	(SELECT hisab_branch FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_branch',
	(SELECT driver_code FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'driver_code',
	(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
	(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-AIR_AND_GREASE') as 'exp_ag',
	(SELECT branch_user FROM dairy.log_hisab WHERE trip_no='$trip_no') as 'branch_user',
	(SELECT tno FROM dairy.log_hisab WHERE trip_no='$trip_no') as 'tno',
	(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'exp_tel'
	");
	
	if(!$sql){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}
	
	if(numRows($sql)==0)
	{
		AlertRightCornerError("No record found !");
		exit();
	}
	
	$row_trip = fetchArray($sql);
	
	$tno = $row_trip['tno'];
	
	$get_driver = Qry($conn,"SELECT name FROM dairy.driver WHERE code='$row_trip[driver_code]'");
	
	if(!$get_driver){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}
	
	$row_driver = fetchArray($get_driver);
	
	$get_user = Qry($conn,"SELECT name FROM emp_attendance WHERE code='$row_trip[branch_user]'");
	
	if(!$get_user){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
		exit();
	}
	
	$row_get_user = fetchArray($get_user);
	
	$driver_name = $row_driver['name'];
	$hisab_user = $row_get_user['name'];
	
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>Vehicle_No</th>
			<th>TripNo</th>
			<th>DriverName</th>
			<th>DA</th>
			<th>SALARY</th>
			<th>Telephone<br>Air-Grease</th>
			<th>Trip</th>
			<th>Trip - Date</th>
			<th>HisabDate</th>
			<th>HisabBranch & User</th>
		</tr>
		</thead>
    <tbody id=""> 
	
	<?php
		$start_date = date('d-m-y', strtotime($row_trip['from']));
		$end_date = date('d-m-y', strtotime($row_trip['to']));
		$hisab_date = date('d-m-y', strtotime($row_trip['hisab_date']));

		if(!empty($row_trip['da_amount']))
		{
			$da_from_date = substr(str_replace('DA-','',str_replace(' ','',trim($row_trip['da_desc']))),0,10);
			$da_to_date = str_replace('to','',str_replace('DA-','',str_replace(' ','',trim($row_trip['da_desc']))));
			$da_to_date = str_replace($da_from_date,'',$da_to_date);
		
			$da_from_date = date("d-m-y",strtotime($da_from_date));
			$da_to_date = date("d-m-y",strtotime($da_to_date));
			
			$da_desc = "<b>$row_trip[da_amount]</b><br>$da_from_date to $da_to_date</b>";
		}
		else
		{
			$da_desc = "";
		}
		
		if(!empty($row_trip['sal_amount']))
		{
			$sal_from_date = substr(str_replace('SALARY-','',str_replace(' ','',trim($row_trip['sal_desc']))),0,10);
			$sal_to_date = str_replace('to','',str_replace('SALARY-','',str_replace(' ','',trim($row_trip['sal_desc']))));
			$sal_to_date = str_replace($sal_from_date,'',$sal_to_date);
		
			$sal_from_date = date("d-m-y",strtotime($sal_from_date));
			$sal_to_date = date("d-m-y",strtotime($sal_to_date));
			
			$sal_desc = "<b>$row_trip[sal_amount]</b><br>$sal_from_date to $sal_to_date</b>";
		}
		else
		{
			$sal_desc = "";
		}
		
		$secret_key = md5($trip_no.$tno.date("ymd"));
		
		echo "<tr>	
			<td>$tno</td>
			<td><button type='button' onclick=ViewHisab('$trip_no','$secret_key') class='btn btn-primary btn-xs'>$trip_no</button></td>
			<td>$driver_name</td>
			<td>$da_desc</td>
			<td>$sal_desc</td>
			<td>
				Telephone : $row_trip[exp_tel]<br>
				Air-Grease : $row_trip[exp_ag]
			</td>
			<td>$row_trip[from_station] to $row_trip[to_station]</td>
			<td>$start_date to $end_date</td>
			<td>$hisab_date</td>
			<td>$row_trip[hisab_branch]<br>($hisab_user)</td>
		</tr>";
	
	echo "</tbody>
</table>";
}
else
{
	if($_POST['duration']!='' AND $tno!='')
	{
		$where_condition = "l.date BETWEEN '$from_date' AND '$to_date' AND l.tno='$tno'";
	}
	else if($_POST['duration']!='' AND $tno=='')
	{
		$where_condition = "l.date BETWEEN '$from_date' AND '$to_date'";
	}
	else if($_POST['duration']=='' AND $tno!='')
	{
		$where_condition = "l.tno='$tno'";
	}
	else
	{
		AlertRightCornerError("Atleast one field is required !");
		exit();
	}
	
	$sql = Qry($conn,"SELECT l.tno,l.trip_no,d.name as driver_name,u.name as hisab_user 
	FROM dairy.log_hisab AS l 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = l.driver 
	LEFT OUTER JOIN emp_attendance AS u ON u.code = l.branch_user
	WHERE $where_condition ORDER BY l.date ASC");
	
	if(!$sql){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($sql)==0)
	{
		AlertRightCornerError("No record found !");
		exit();
	}
	
	?>
	<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_No</th>
			<th>TripNo</th>
			<th>DriverName</th>
			<th>DA</th>
			<th>SALARY</th>
			<th>Telephone<br>Air-Grease</th>
			<th>Trip</th>
			<th>Trip - Date</th>
			<th>HisabDate</th>
			<th>HisabBranch & User</th>
		</tr>
		</thead>
    <tbody id=""> 
	<?php
	$sn=1;
	while($row1 = fetchArray($sql))
	{
		$tno = $row1['tno'];
		$trip_no = $row1['trip_no'];
		$driver_name = $row1['driver_name'];
		$hisab_user = $row1['hisab_user'];
		
		$sql1 = Qry($conn,"SELECT 
		(SELECT date(date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from',
		(SELECT date(end_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to',
		(SELECT from_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'from_station',
		(SELECT to_station FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id DESC LIMIT 1) as 'to_station',
		(SELECT date(hisab_date) FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_date',
		(SELECT hisab_branch FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'hisab_branch',
		(SELECT driver_code FROM dairy.trip_final WHERE trip_no='$trip_no' ORDER BY id ASC LIMIT 1) as 'driver_code',
		(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_desc',
		(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'DA%') as 'da_amount',
		(SELECT desct FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_desc',
		(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct like 'SALARY%') as 'sal_amount',
		(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-AIR_AND_GREASE') as 'exp_ag',
		(SELECT debit FROM dairy.driver_book WHERE trip_no='$trip_no' AND desct='EXP-TELEPHONE') as 'exp_tel'
		");
		
		if(!$sql1){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
			exit();
		}
		
		$row_trip = fetchArray($sql1);
		
		if(!empty($row_trip['da_amount']))
		{
			$da_from_date = substr(str_replace('DA-','',str_replace(' ','',trim($row_trip['da_desc']))),0,10);
			$da_to_date = str_replace('to','',str_replace('DA-','',str_replace(' ','',trim($row_trip['da_desc']))));
			$da_to_date = str_replace($da_from_date,'',$da_to_date);
		
			$da_from_date = date("d-m-y",strtotime($da_from_date));
			$da_to_date = date("d-m-y",strtotime($da_to_date));
			
			$da_desc = "<b>$row_trip[da_amount]</b><br>$da_from_date to $da_to_date</b>";
		}
		else
		{
			$da_desc = "";
		}
		
		if(!empty($row_trip['sal_amount']))
		{
			$sal_from_date = substr(str_replace('SALARY-','',str_replace(' ','',trim($row_trip['sal_desc']))),0,10);
			$sal_to_date = str_replace('to','',str_replace('SALARY-','',str_replace(' ','',trim($row_trip['sal_desc']))));
			$sal_to_date = str_replace($sal_from_date,'',$sal_to_date);
		
			$sal_from_date = date("d-m-y",strtotime($sal_from_date));
			$sal_to_date = date("d-m-y",strtotime($sal_to_date));
			
			$sal_desc = "<b>$row_trip[sal_amount]</b><br>$sal_from_date to $sal_to_date</b>";
		}
		else
		{
			$sal_desc = "";
		}
		
		$start_date = date('d-m-y', strtotime($row_trip['from']));
		$end_date = date('d-m-y', strtotime($row_trip['to']));
		$hisab_date = date('d-m-y', strtotime($row_trip['hisab_date']));

		$secret_key = md5($trip_no.$tno.date("ymd"));
		
		echo "<tr>	
			<td>$sn</td>
			<td>$tno</td>
			<td><button type='button' onclick=ViewHisab('$trip_no','$secret_key') class='btn btn-primary btn-xs'>$trip_no</button></td>
			<td>$driver_name</td>
			<td>$da_desc</td>
			<td>$sal_desc</td>
			<td>
				Telephone : $row_trip[exp_tel]<br>
				Air-Grease : $row_trip[exp_ag]
			</td>
			<td>$row_trip[from_station] to $row_trip[to_station]</td>
			<td>$start_date to $end_date</td>
			<td>$hisab_date</td>
			<td>$row_trip[hisab_branch]<br>($hisab_user)</td>
		</tr>";
		$sn++;
	}
	
echo "</tbody>
</table>";
}
?>
	
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 
