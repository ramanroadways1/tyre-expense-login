<?php
require_once '_connect.php'; 

mysqli_set_charset($conn, 'utf8');

$tno = escapeString($conn,($_POST['tno']));

if($tno=='')
{
	AlertRightCornerError("Enter vehicle number first !");
	exit();
}
	
$sql = Qry($conn,"SELECT a.item_code,a.item_qty,a.item_rate,IF(a.item_value_up='1','Given','Not Given') as is_given,
a.branch,a.trip_no,a.timestamp,i.item_name,d.name 
FROM dairy.asset_form AS a 
LEFT JOIN dairy.asset_form_items AS i ON i.code=a.item_code
LEFT JOIN dairy.driver AS d ON d.code = a.driver_code
WHERE a.tno='$tno'");

$d_name="NA";
$timestamp="NA";
	
if(!$sql){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	
?>
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        
		<tr><th colspan="7" id="top_row"></th></tr>
		
		<tr>
			<th>Item_Name</th>
			<th>Item_Code</th>
			<th>Qty</th>
			<th>Rate</th>
			<th>Is_Given</th>
			<th>Filled_by_branch</th>
			<th>Trip_No</th>
		</tr>
	<?php
	$sn=1;
	while($row = fetchArray($sql))
	{
		$d_name = $row['name'];
		
		if($row['is_given']=='Given'){
			$is_given="<font color='green'>Given</font>";
		}
		else{
			$is_given="<font color='red'>not Given</font>";
		}
		
		$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
		
		echo "<tr>	
				<td>$row[item_name]</td>
				<td>$row[item_code]</td>
				<td>$row[item_qty]</td>
				<td>$row[item_rate]</td>
				<td>$is_given</td>
				<td>$row[branch]</td>
				<td>$row[trip_no]</td>
		</tr>";
		$sn++;
	}
	
echo "
</table>";

echo "<script>
	$('#top_row').html('Driver : $d_name , Fill Timestamp : $timestamp');
</script>";
?>
	
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 
