<?php
require_once '_connect.php';

$output ='';

$from_date = escapeString($conn,($_REQUEST['from_date']));
$to_date = escapeString($conn,($_REQUEST['to_date']));
$tno = escapeString($conn,($_REQUEST['tno']));

$sql = Qry($conn,"SELECT t.trip_no,t.tno,t.branch,t.edit_branch,t.from_station,t.to_station,t.act_wt,t.charge_wt,t.lr_type,t.lrno,t.date,t.end_date,
d.name as driver_name FROM diesel_api.all_trips AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
WHERE t.date>='$from_date' AND t.end_date<='$to_date' AND t.tno='$tno'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}

if(numRows($sql)==0){
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

$output .= '<table border="1">  
      <tr>  
			<th>SN</th>
			<th>Trip_No</th>
			<th>Driver</th>
			<th>Branch</th>
			<th>Edit_Branch</th>
			<th>From</th>
			<th>To</th>
			<th>Act_Wt</th>
			<th>Chrg_Wt</th>
			<th>LR_Type</th>
			<th>LR_No</th>
			<th>Trip_Date</th>
			<th>End_Date</th>		 
      <tr>';

$sn=1;  
while($row = fetchArray($sql))
{
	$trip_date = date('d-m-y', strtotime($row['date']));
		
		if($row['end_date']!=0){
			$end_date = date('d-m-y', strtotime($row['end_date']));
		}else{
			$end_date = "<font color='green'>Running</font>";
		}
		
	$output .= '<tr> 
		<td>'.$sn.'</td>  
		<td>'.$row["trip_no"].'</td>  
		<td>'.$row["driver_name"].'</td>  
		<td>'.$row["branch"].'</td>  
		<td>'.$row["edit_branch"].'</td>  
		<td>'.$row["from_station"].'</td>  
		<td>'.$row["to_station"].'</td>  
		<td>'.$row["act_wt"].'</td>  
		<td>'.$row["charge_wt"].'</td>  
		<td>'.$row["lr_type"].'</td>  
		<td>'.$row["lrno"].'</td>  
		<td>'.$trip_date.'</td>  
		<td>'.$end_date.'</td>  
	</tr>';
$sn++;
}
  
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Trip_Report_'.$tno.'_'.$from_date.'_To_'.$to_date.'.xls');
  echo $output;	  
?>